﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Miproyectofinal._1.Startup))]
namespace Miproyectofinal._1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
